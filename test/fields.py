import pytest
import base64

from classforge import Class, StrictClass, Field, Base64, Numeric

class Base64Test(Class):

    foo = Base64(default='quack')

def test_subfield():

    obj = Base64Test(foo='Happy Birthday!')
    serialized = obj.to_dict()

    assert serialized['foo'] == b'SGFwcHkgQmlydGhkYXkh'
    obj2 = Base64Test.from_dict(serialized)
    assert obj.foo == "Happy Birthday!"


# --------------------------------------------------------------

class NumericTest(Class):

    a = Numeric(default=5)
    b = Numeric(places=0, max=3000) # foosball=22)
    c = Numeric(min=0, max=10, inclusive=False)
    d = Numeric(min=0, max=10, inclusive=True)
    e = Numeric(type=float, places=5, min=-22, max=22)
    f = Numeric(type=float, places=5, min=-11, max=22, fold=True)

def test_integer_field():

    obj1 = NumericTest(a=3.6, b=3.7, c=5.2, d=6)
    assert obj1.a == 3
    assert obj1.b == 4
    assert obj1.c == 5

    # min/max
    obj2 = NumericTest(c=9)
    obj3 = NumericTest(d=10)
    obj4 = NumericTest(c=1)
    obj5 = NumericTest(d=0)
    assert obj2.c == 9
    assert obj3.d == 10
    assert obj4.c == 1
    assert obj5.d == 0

    # more min/max with inclusion tests
    with pytest.raises(ValueError) as e_info:
        obj5 = NumericTest(c=0)
    with pytest.raises(ValueError) as e_info:
        obj5 = NumericTest(c=10)
    with pytest.raises(ValueError) as e_info:
        obj5 = NumericTest(d=-1)
    with pytest.raises(ValueError) as e_info:
        obj5 = NumericTest(d=11)

    # place rounding for floats
    obj9 = NumericTest(f=1.23456789)
    assert obj9.f == 1.23457

    # folding
    obj10 = NumericTest(f=99)
    assert obj10.f == 22
    obj11 = NumericTest(f=-3000)
    assert obj11.f == -11

def test_numeric_explain():

    obj1 = NumericTest(a=3.6, b=3.7, c=5.2, d=6)
    explain = obj1.explain()
    assert explain['a']['default'] == 5
    assert explain['c']['min'] == 0
    assert explain['d']['inclusive'] == True
    assert explain['d']['required'] == False
    assert explain['d']['mutable'] == True
    assert explain['d']['_class'] == 'Numeric'